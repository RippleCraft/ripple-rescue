package ripplecraft.ripple_rescue.bean;

import org.bukkit.entity.Item;

// TODO: 2021/5/14 需要完成商品物品类的设计以及json的读入输出
public class ShopItem {
    private Integer id;
    private String title;
    private String describe;
    private Integer amount;
    /**
     * 价格单位：兑换币
     */
    private Integer price;
    /**
     * 表示物品的种类：
     * 0.普通材料（石头、木头等）；
     * 1.矿物（包括各种锭）；
     * 2.生物战利品；
     * 3.农作物；
     * 4.装备；
     * 5.mod中对发展有利的物品（或材料）
     * 6.特殊物品
     */
    private Integer type;

}
