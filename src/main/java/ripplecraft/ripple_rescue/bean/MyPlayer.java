package ripplecraft.ripple_rescue.bean;

import com.google.gson.Gson;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;

public class MyPlayer {
    public final static String fileAddress = "plugins/RippleRescue/player/";
    private Player player;
    private String playerUUID;
    private MyPlayerData data;

    public MyPlayer(Player player) {
        this.player = player;
        this.playerUUID = player.getUniqueId().toString();

        File file = new File(this.fileAddress + this.playerUUID + ".json");
        //如果json存在，从json中创建
        if(file.exists()){
            try {
                this.data = GetDataFromJson(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //否则初始化
        else{
            this.data = new MyPlayerData();
            this.data.donateCount = 0;
        }
        this.data.playerName = player.getName();
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getPlayerName() {
        return this.data.playerName;
    }

    public void setPlayerName(String playerName) {
        this.data.playerName = playerName;
    }

    public String getPlayerUUID() {
        return this.playerUUID;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    public int getDonateCount(){ return this.data.donateCount; }

    public void addDonateCounts(int donateCount){
        this.data.donateCount += donateCount;
        SaveToJsonFile();
    }

    private static MyPlayerData GetDataFromJson(File file) throws IOException{
        StringBuffer jsonStrBuf = new StringBuffer();
        String tmp;
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        while ((tmp = reader.readLine()) != null) {
            jsonStrBuf.append(tmp);
        }
        return new Gson().fromJson(jsonStrBuf.toString(), MyPlayerData.class);
    }

    /**
     * 将数据保存到uuid.json文件中
     */
    public void SaveToJsonFile() {
        try {
            new File(this.fileAddress).mkdirs();
            File file = new File(this.fileAddress+this.playerUUID+".json");
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file));
            osw.write(new Gson().toJson(this.data));
            osw.flush();
            osw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取贡献值排行榜
     * @return 贡献值排行榜字符串
     */
    public static String GetDonateRankList(){
        //读取全部数据
        ArrayList<MyPlayerData> dataArrayList = new ArrayList<MyPlayerData>();
        try{
            File[] list = new File(MyPlayer.fileAddress).listFiles();
            for(File file : list){
                if(file.isFile() && file.getName().endsWith(".json")){
                    dataArrayList.add(GetDataFromJson(file));
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        //排序
        dataArrayList.sort(Comparator.reverseOrder());

        //制作排行榜字符串
        String result = ChatColor.YELLOW + "==========贡献值排行榜==========\n" + ChatColor.BLUE;
        for(int i = 0;i < dataArrayList.size();i++){
            result += (i+1) + ". " + dataArrayList.get(i).playerName + "   " + dataArrayList.get(i).donateCount + '\n';
        }
        return result + ChatColor.YELLOW + "=================================";
    }
}
