package ripplecraft.ripple_rescue.bean;

import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

public class MyItemStack {
    private int type;
    private int amount;
    private MaterialData data;
    private short durability;
    private ItemMeta meta;

    public MyItemStack(int type, int amount, MaterialData data, short durability,ItemMeta meta) {
        this.type = type;
        this.amount = amount;
        this.data = data;
        this.durability = durability;
        this.meta = meta;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public MaterialData getData() {
        return data;
    }

    public void setData(MaterialData data) {
        this.data = data;
    }

    public short getDurability() {
        return durability;
    }

    public void setDurability(short durability) {
        this.durability = durability;
    }

    public ItemMeta getMeta() {
        return meta;
    }

    public void setMeta(ItemMeta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "{" +
                "type=" + type +
                ", amount=" + amount +
                ", data=" + data +
                ", durability=" + durability +
                ", meta=" + meta +
                '}';
    }
}
