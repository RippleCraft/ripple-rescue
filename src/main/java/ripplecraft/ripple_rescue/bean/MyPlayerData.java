package ripplecraft.ripple_rescue.bean;

public class MyPlayerData implements Comparable<MyPlayerData>{
    public String playerName;
    public int donateCount;

    @Override
    public int compareTo(MyPlayerData o) {
        return this.donateCount - o.donateCount;
    }
}
