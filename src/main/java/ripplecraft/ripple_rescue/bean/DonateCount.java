package ripplecraft.ripple_rescue.bean;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class DonateCount {
    /**
     * @param player 贡献的玩家
     * @param material 贡献的物品
     * @return 返回该玩家贡献值
     */
    public int CountNow(Player player, Material material){
        HashMap<Integer, ? extends ItemStack> map;
        map = player.getInventory().all(material);//取到这个物品的map
        Set<Integer> keySet = map.keySet();
        Iterator<Integer> it = keySet.iterator();
        int allCount = 0;

        while (it.hasNext()) {
            Integer key = it.next();
            allCount = allCount + map.get(key).getAmount();//结算至全服贡献
            player.getInventory().setItem(key, new ItemStack(Material.AIR));//清空背包内相应的格子
        }
        return allCount;
    }
    }
