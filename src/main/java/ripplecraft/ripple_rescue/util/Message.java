package ripplecraft.ripple_rescue.util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.command.CommandSender;

public enum Message {
    not_a_player("[RippleRescue]这个指令只有玩家才能使用~"),
    enter_error("[RippleRescue]输入了未知指令，请检查输入~"),
    error("[RippleRescue]发生错误！"),
    menu_info(ChatColor.GREEN+"===========涟漪救援·主菜单===========" +
            "\nrr i")
    ;

    public String s;

    Message(String s) {
        this.s = s;
    }

    public static void sendMessageToSender(CommandSender sender, Log log, String msg) {
        ChatColor color = null;
        switch (log){
            case success:
                color = ChatColor.GREEN;
                break;
            case warm:
                color = ChatColor.YELLOW;
                break;
            case error:
                color = ChatColor.RED;
                break;
            case debug:
                color = ChatColor.BLUE;
                break;
        }
        sender.sendMessage(color+ msg);
    }

    public static void error(String s) {
        Bukkit.getLogger().info(ChatColor.RED + "发生错误！");
        Bukkit.getLogger().info(ChatColor.RED+ s);
    }

    public static void warn(String s) {
        Bukkit.getLogger().info(ChatColor.GOLD + "警告：");
        Bukkit.getLogger().info(ChatColor.GOLD + s);
    }


    public static void debug(String s) {
        Bukkit.getLogger().info(ChatColor.YELLOW+ s);
    }

    public static void success(String s) {
        Bukkit.getLogger().info(ChatColor.GREEN+ s);
    }
}
