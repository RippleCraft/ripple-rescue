package ripplecraft.ripple_rescue.util;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ripplecraft.ripple_rescue.module.*;
import ripplecraft.ripple_rescue.module.Module;

public class CommandProcessor {
    private String[] args;
    private CommandSender sender;

    /**
     * @param sender 指令发起者
     * @param args   指令字符串
     */
    public CommandProcessor(CommandSender sender, String[] args) {
        this.args = args;
        this.sender = sender;
    }

    public void processor() {
        if (this.args.length != 0)
            if (sender instanceof Player) commandSwitch();
            else Message.sendMessageToSender(sender, Log.warm, Message.not_a_player.s);
        else Message.sendMessageToSender(sender, Log.error, Message.enter_error.s);
    }

    /**
     * 通过玩家输入的指令选择合适的模块执行
     */
    private void commandSwitch() {
        Module module;
        switch (args[0]) {
            //背包类启用后才会实装的语句
            /* case "b":
                module = new BackPackModule((Player) sender);
                break;*/
            case "i":
                module = new MyInfoModule((Player) sender);
                break;
            case "d":
                module = new DonateModule((Player) sender);
                break;
            case "p":
                module = new PotionModule((Player) sender);
                break;
            case "shop":
                module = new ShopModule((Player) sender);
                break;
            default:
                module = new NullModule((Player) sender);
                break;
        }
        module.function();
    }
}
