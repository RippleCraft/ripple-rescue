package ripplecraft.ripple_rescue.listener;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class ListenerRegister {
    private Plugin plugin;

    public ListenerRegister(Plugin plugin) {
        this.plugin = plugin;
    }

    public void registerAll() {
        Bukkit.getServer().getPluginManager().registerEvents(new InventoryListener(),plugin);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(),plugin);
    }
}
