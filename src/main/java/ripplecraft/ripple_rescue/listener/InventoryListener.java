
package ripplecraft.ripple_rescue.listener;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ripplecraft.ripple_rescue.bean.DonateCount;
import ripplecraft.ripple_rescue.bean.MyPlayer;


public class InventoryListener implements Listener {
    private Player player;
    private ItemStack[] contents;
    private Inventory inventory;

    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        this.player = (Player) event.getPlayer();
        this.inventory = event.getInventory();
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        /*
        首先if判断打开的容器是否为捐献菜单，如果是则继续执行，否则返回
        然后再判断一下玩家按下的是哪个按钮
        如果是捐献按钮，则直接判断玩家背包内是否有捐献所用的材料，如果有则统计并扣除，并把统计数据加到全服贡献值去
        如果是进度按钮，则在聊天框中显示全服贡献值进度
        如果是排行按钮，则在聊天框中显示全服贡献值前五的玩家ID，并提示用指令+页码可以翻页
        */

        if (event.getInventory().getTitle().equals("材料贡献菜单")) {
            event.setCancelled(true);//防止玩家把菜单里东西拿出来了
            switch (event.getRawSlot()) {
                //0是捐献按钮
                //1是查看贡献值
                //2是查看排行榜
                case 0 : {
                    MyPlayer myPlayer = new MyPlayer(player);
                    DonateCount dc = new DonateCount();
                    String str = player.getDisplayName() + ChatColor.GREEN + " 获得了" + ChatColor.BLUE;
                    if (player.getInventory().contains(Material.APPLE)) {//判断玩家背包内是否有某物
                        int count1 = dc.CountNow(player, Material.APPLE);
                        myPlayer.addDonateCounts(count1);
                        player.sendMessage(str + count1 + ChatColor.GREEN + " 点苹果贡献值");
                    }
                    if (player.getInventory().contains(Material.ARROW)) {
                        int count2 = dc.CountNow(player, Material.ARROW);
                        myPlayer.addDonateCounts(count2);
                        player.sendMessage(str + count2 + ChatColor.GREEN + " 点弓箭贡献值");
                    }
                    if (player.getInventory().contains(Material.BED)) {
                        int count3 = dc.CountNow(player, Material.BED);
                        myPlayer.addDonateCounts(count3);
                        player.sendMessage(str + count3 + ChatColor.GREEN + " 点床铺贡献值");
                    }
                    break;
                }
                case 1: {
                    MyPlayer myPlayer = new MyPlayer(player);
                    player.sendMessage(ChatColor.GREEN + "当前贡献值: "+ myPlayer.getDonateCount());
                    break;
                }
                case 2:
                    player.sendMessage(MyPlayer.GetDonateRankList());
                    break;
            }

        }
    }
}
