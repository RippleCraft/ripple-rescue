package ripplecraft.ripple_rescue;


import org.bukkit.ChatColor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import org.bukkit.plugin.java.JavaPlugin;

import ripplecraft.ripple_rescue.listener.ListenerRegister;
import ripplecraft.ripple_rescue.util.CommandProcessor;


public final class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        //注册所有监听器
        new ListenerRegister(this).registerAll();
        this.getLogger().info(ChatColor.AQUA +"=====涟漪救援插件已启动=====");
        this.getLogger().info(ChatColor.AQUA +"=====作者：涟漪服务器=====");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equals("rr")) new CommandProcessor(sender, args).processor();
        return false;
    }

    @Override
    public void onDisable() {

    }
}
