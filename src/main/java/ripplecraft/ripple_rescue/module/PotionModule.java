package ripplecraft.ripple_rescue.module;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionModule extends Module{
    private Player player;

    public PotionModule(Player player) {
        this.player = player;
    }

    @Override
    public void function() {
        PotionEffect potionEffect = new PotionEffect(PotionEffectType.JUMP,5000,1);
        potionEffect.apply(player);
    }
}
