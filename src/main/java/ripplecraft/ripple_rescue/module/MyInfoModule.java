package ripplecraft.ripple_rescue.module;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ripplecraft.ripple_rescue.bean.MyPlayer;
import ripplecraft.ripple_rescue.util.Message;

import java.io.IOException;

public class MyInfoModule extends Module {
    private Player player;

    public MyInfoModule(Player player) {
        this.player = player;
    }

    @Override
    public void function() {
        MyPlayer myPlayer = new MyPlayer(player);
        //背包类启用后才实装的语句
        /*String msg = "玩家名："+player.getName()+
                "\n玩家id："+player.getUniqueId().toString()+
                "\n可用的插件背包空间："+myPlayer.getBackPackSize()+
                "\n";*/
        String msg = "玩家名："+player.getName()+
                "\n玩家id："+player.getUniqueId().toString()+
                "\n";
        player.sendMessage(ChatColor.YELLOW + msg);
    }
}
