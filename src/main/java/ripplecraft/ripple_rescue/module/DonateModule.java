package ripplecraft.ripple_rescue.module;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ripplecraft.ripple_rescue.bean.MyPlayer;
import ripplecraft.ripple_rescue.util.Log;
import ripplecraft.ripple_rescue.util.Message;

import java.io.IOException;

public class DonateModule extends Module{
    private Player player;
    private MyPlayer myPlayer;
    private Inventory donateInventory;

    public DonateModule(Player player) {
        this.player = player;
        this.myPlayer = new MyPlayer(player);
//        try {
//            //尝试通过player的uuid查找、构建对应的MyPlayer对象
//            this.myPlayer = MyPlayer.getInJsonFile(this.player.getUniqueId());
//        } catch (IOException e) {
//            Message.sendMessageToSender(player, Log.error,Message.error.s);
//            Message.error(e.getMessage());
//            e.printStackTrace();
//        }
    }

    @Override
    public void function() {
        if (this.myPlayer != null) {
            openDonateMenu();
        }
    }

    //打开材料捐献界面
    void openDonateMenu(){
        Inventory inventory = Bukkit.createInventory(player, 9,"材料贡献菜单");
        donateInventory = inventory;

        //到时候把Material改成自定义的物品即可
        ItemStack donate = new ItemStack(Material.APPLE);
        ItemStack donateValue = new ItemStack(Material.BONE);
        ItemStack donateRanking = new ItemStack(Material.DIAMOND);

        inventory.setItem(0,donate);//从0开始数格子...
        inventory.setItem(1,donateValue);
        inventory.setItem(2,donateRanking);

        player.openInventory(inventory);
    }
    public Inventory getDonateInventory(){
        return donateInventory;
    }
}
