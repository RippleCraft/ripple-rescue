package ripplecraft.ripple_rescue.module;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopModule extends Module{
    private Player player;

    public ShopModule(Player player) {
        this.player = player;
    }

    // TODO: 2021/5/14 需要完成商店的扣款功能和商品信息展示功能
    @Override
    public void function() {
        Inventory shop = getInventory(player, 54, "每日商店");
        String name = "appliedenergistics2_tiny_tnt";
        ItemStack testWood = new ItemStack(Material.valueOf(name.toUpperCase()), 8);
        shop.addItem(testWood);
        player.openInventory(shop);

    }
}
