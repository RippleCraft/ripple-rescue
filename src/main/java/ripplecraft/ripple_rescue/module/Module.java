package ripplecraft.ripple_rescue.module;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public abstract class Module {

    public abstract void function();

    protected Inventory getInventory(Player player, int size, String title){
        return Bukkit.createInventory(player, size, title);
    }
}
