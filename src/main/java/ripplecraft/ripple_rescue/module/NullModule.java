package ripplecraft.ripple_rescue.module;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ripplecraft.ripple_rescue.module.Module;
import ripplecraft.ripple_rescue.util.Log;
import ripplecraft.ripple_rescue.util.Message;

public class NullModule extends Module {
    private Player player;

    public NullModule(Player player) {
        this.player = player;
    }

    @Override
    public void function() {
        Message.sendMessageToSender((CommandSender) player, Log.error,Message.enter_error.s);
    }
}
