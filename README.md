# 涟漪救援

#### 介绍
涟漪救援是为了涟漪的超级周目而打造，计划集成的功能有：
1. 捐材料模块，包括待捐的物品、数量、图标，包括捐材料可以获得的奖励、成就以及排行榜等。
   
2. 兑换币商店模块，包括商店的物品、价格、数量、图标，要实现每天限量随机提供一系列固定种类的物品，需要每日自动刷新。带有10连抽奖功能，**某些特殊物品只能通过兑换币商店或抽奖获得**。
   
3. 每日（周）任务模块，玩家通过每日任务也可以获得一些兑换币，每日任务同样会通过一系列数据自动刷新。

4. 通行证模块，玩家完成特定的任务可以提高通行证等级，每达到一个通行证等级都能够获取材料、装备、附魔书等等以及一些独特的物品。

5. 自定义药水效果系统。
#### 已实现的模块
1. 死亡惩罚模块。

